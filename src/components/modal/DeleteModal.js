import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button, Modal } from "react-bootstrap";
import { connectModal } from "redux-modal";

class DeleteModal extends Component {
  static propTypes = {
    handleHide: PropTypes.func.isRequired,
    deleteConfirmed: PropTypes.func.isRequired
  };
  
  render() {
    const { show, handleHide } = this.props;

    return (
      <Modal show={show}>
        <Modal.Header>
          <Modal.Title>Danger - You are about to delete card</Modal.Title>
        </Modal.Header>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleHide}>
            Cancel
          </Button>
          <Button variant="danger" onClick={this.props.deleteConfirmed}>
            Confirme Delete
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default connectModal({ name: "deleteModal" })(DeleteModal);

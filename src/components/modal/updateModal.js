import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button, Modal, Form } from "react-bootstrap";
import { connectModal } from "redux-modal";

class updateModal extends Component {
  static propTypes = {
    handleHide: PropTypes.func.isRequired,
    updateCard: PropTypes.func.isRequired
  };
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  onSubmit(e) {
    e.preventDefault();
    Object.assign(this.props.card, this.state)
    this.props.updateCard(this.props.cards);
  }
  render() {
    const { show, handleHide } = this.props;

    return (
      <Modal show={show}>
        <Modal.Header>
          <Modal.Title>Update Card</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={this.onSubmit}>
            <Form.Group controlId="formTitle">
              <Form.Label>Title</Form.Label>
              <Form.Control
                onChange={this.onChange}
                name="title"
                type="text"
                placeholder="Enter title"
              />
            </Form.Group>
            <Form.Group controlId="formDesctiption">
              <Form.Label>Desctiption</Form.Label>
              <Form.Control
                name="description"
                type="text"
                placeholder="Enter description"
                onChange={this.onChange}
              />
            </Form.Group>
            <Form.Group controlId="formLastUpdated">
              <Form.Label>Enter last updated</Form.Label>
              <Form.Control
                name="lastUpdated"
                type="text"
                placeholder="Last updated"
                onChange={this.onChange}
              />
            </Form.Group>
            <Form.Group controlId="formRole">
              <Form.Label>Enter Role</Form.Label>
              <Form.Control
                onChange={this.onChange}
                name="role"
                type="text"
                placeholder="Role"
              />
            </Form.Group>
            <Button variant="primary" type="submit">
              Submit
            </Button>
            <Button variant="secondary" onClick={handleHide}>
              Cancel
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

export default connectModal({ name: "updateModal" })(updateModal);

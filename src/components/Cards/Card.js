//React import
import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { setCurrentCard } from "./cardsActions";

// //External Import
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeartbeat, faPen, faTimes } from "@fortawesome/free-solid-svg-icons";
import "bootstrap/dist/css/bootstrap.css";
import "./card.css";

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: this.props.card.title,
      description: this.props.card.description,
      lastUpdated: this.props.card.lastUpdated,
      isHover: false
    };
    this.handleMouseEvent = this.handleMouseEvent.bind(this);
    this.onEditClicked = this.onEditClicked.bind(this);
    this.onDeleteClicked = this.onDeleteClicked.bind(this);
  }

  handleMouseEvent() {
    this.state.isHover = !this.state.isHover;
    this.setState(this.state);
  }
  onEditClicked() {
    this.props.setCurrentCard(this.props.card);
    this.props.showModal("updateModal");
  }
  onDeleteClicked() {
    this.props.setCurrentCard(this.props.card);
    this.props.showModal("deleteModal");
  }
  render() {
    return (
      <div>
        <div
          className="cardContainerStyle"
          onMouseEnter={this.handleMouseEvent}
          onMouseLeave={this.handleMouseEvent}
        >
          <div className="d-flex flex-column">
            <div className="d-flex justify-content-between cardTitleContainer">
              <div>
                <FontAwesomeIcon icon={faHeartbeat} size="lg" color="#9A0000" />
                <h5 className="card-title cardTitleStyle">
                  {this.props.card.title}
                </h5>
              </div>
              <div className="d-flex justify-content-between cardIcons">
                <FontAwesomeIcon
                  icon={faPen}
                  size="xs"
                  className={this.state.isHover ? "showElement" : "hideElement"}
                  onClick={this.onEditClicked}
                />
                <FontAwesomeIcon
                  icon={faTimes}
                  className={this.state.isHover ? "showElement" : "hideElement"}
                  onClick={this.onDeleteClicked}
                />
              </div>
            </div>
            <p className="cardDescriptionStyle">
              {this.props.card.description}
            </p>
            <div className="d-flex justify-content-between cardUpdateRoleContainer">
              <p>{this.props.card.lastUpdated}</p>
              <p>{this.props.card.role}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Card.propTypes = {
  card: PropTypes.object.isRequired,
  cardMouseHover: PropTypes.func,
  cardMouseLeave: PropTypes.func,
  deleteCard: PropTypes.func,
  showModal: PropTypes.func,
  setCurrentCard: PropTypes.func
};
const mapDispatchToProps = dispatch => {
  return {
    setCurrentCard: card => dispatch(setCurrentCard(card))
  };
};
export default connect(
  null,
  mapDispatchToProps
)(Card);

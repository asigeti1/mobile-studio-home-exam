import {
  FETCH_CARDS,
  DELETE_CARD,
  UPDATE_CARD,
  SET_CURRENT_CARD,
  FETCH_CARDS_DONE
} from "./types";

const initialState = {
  items: [],
  item: {},
  isLoading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    //start fetch cards 
    case FETCH_CARDS:
      return {
        ...state,
        isLoading: action.isLoading
      };
      //done fatching cards
    case FETCH_CARDS_DONE:
      return {
        ...state,
        items: action.payload,
        isLoading: action.isLoading
      };
    case DELETE_CARD:
      return {
        ...state,
        items: state.items.filter(item => action.payload !== item)
      };
    case UPDATE_CARD:
      return {
        ...state,
        item: action.payload
      };
      //set cards that is currently neeng edit/delete
    case SET_CURRENT_CARD:
      return {
        ...state,
        item: action.payload
      };
    default:
      return state;
  }
}

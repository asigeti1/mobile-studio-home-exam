import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { fetchCards, updateCard, deleteCard } from "./cardsActions";
import { show, hide } from "redux-modal";
import Card from "./Card";
import DeleteModal from "../modal/DeleteModal";
import UpdateModal from "../modal/updateModal";
import "bootstrap/dist/css/bootstrap.css";
import "./card.css";

class Cards extends Component {
  componentWillMount() {
    //fetch cards on component load 
    this.props.fetchCards();
  }

  handleOpenModal(name) {
    this.props.show(name, { message: `` });
  }

  updateCard() {
    this.props.hide("updateModal");
    this.props.updateCard();
  }

  deleteCard() {
    if (this.props.card) {
      this.props.hide("deleteModal");
      this.props.deleteCard(this.props.card);
    }
  }
 
  render() {
    return (
      <div className="d-flex flex-wrap cardsContainer ">
      <div className={this.props.isCardsLoading? "showSpinnerElement" : "hideElement"}>
          <div className="d-flex justify-content-center spinner">
            <div className="spinner-border" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        </div>
        {/* itirate thru cards list */}
        {this.props.cards.map(card => (
          <Card
            key={card.id}
            card={card}
            showModal={name => this.handleOpenModal(name)}
          />
        ))}
        
        <DeleteModal deleteConfirmed={this.deleteCard.bind(this)} />
        <UpdateModal
          cards={this.props.cards}
          card={this.props.card}
          updateCard={this.updateCard.bind(this)}
        />
      </div>
    );
  }
}

Cards.propTypes = {
  fetchCards: PropTypes.func.isRequired,
  cards: PropTypes.array.isRequired,
  showModal: PropTypes.func,
  updateCard: PropTypes.func
};

const mapStateToProps = state => ({
  cards: state.cards.items,
  card: state.cards.item,
  isCardsLoading: state.cards.isLoading
});
const mapDispatchToProps = dispatch => {
  return {
    hide: name => dispatch(hide(name)),
    show: name => dispatch(show(name)),
    fetchCards: () => dispatch(fetchCards()),
    updateCard: cards => dispatch(updateCard(cards)),
    deleteCard: card => dispatch(deleteCard(card))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cards);

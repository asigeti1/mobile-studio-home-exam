import axios from "axios";
import {
  FETCH_CARDS,
  FETCH_CARDS_DONE,
  UPDATE_CARD,
  DELETE_CARD,
  SET_CURRENT_CARD
} from "./types";
//url For internal Mock server 
const mockPath = "http://localhost:3001/cards";
//Cards Actions 
export const fetchCards = () => {
  return dispatch => {
    dispatch(cardsIsLoading(true))
    return axios.get(mockPath).then(response => {
      dispatch(syncFetchCard(response.data));
    });
  };
};
export const fetchCardsDone = () => {
  return {
    type: FETCH_CARDS_DONE
  };
};
export const updateCard = cards => {
  return {
    type: UPDATE_CARD,
    payload: cards
  };
};
export const deleteCard = card => {
  return {
    type: DELETE_CARD,
    payload: card
  };
};

export const setCurrentCard = card => {
  return {
    type: SET_CURRENT_CARD,
    payload: card
  };
};


//Internal function 
function syncFetchCard(cards) {
  return {
    type: FETCH_CARDS_DONE,
    payload: cards,
    isLoading: false
  };
}
function cardsIsLoading(bool) {
  return {
      type: FETCH_CARDS,
      isLoading: bool
  };
}

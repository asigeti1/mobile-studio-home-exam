import { combineReducers } from 'redux';
import cardsReducer from '../components/Cards/cardsReducer';
import { reducer as modal } from 'redux-modal';


export default combineReducers({
  cards: cardsReducer,
  modal
});

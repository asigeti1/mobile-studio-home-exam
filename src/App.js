import React, { Component } from "react";
import "./App.css";
import { Provider } from "react-redux";
import store from "./store";
import Cards from "./components/Cards/Cards";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App-header">
          <h1>Mobile Studio – Home Exam </h1>
          <Cards />
        </div>
      </Provider>
    );
  }
}

export default App;
